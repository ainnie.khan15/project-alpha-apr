"""
URL configuration for tracker project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from .views import redirect_view
from accounts.views import login, sign_out
from projects.views import list_projects, show_project

urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", list_projects, name="list_projects"),
    path("projects/<int:id>/", show_project, name="show_project"),
    path("accounts/login/", login, name="login"),
    path("", redirect_view, name="home"),
    path("accounts/logout/", sign_out, name="logout"),
    path("tasks/", include("tasks.urls")),
    path("projects/", include("projects.urls")),
]
