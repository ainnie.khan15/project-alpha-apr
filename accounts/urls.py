from django.urls import path

from .views import login, sign_out, signup

app_name = "accounts"

urlpatterns = [
    path("login/", login, name="login"),
    path("logout/", sign_out, name="logout"),
    path("register/", signup, name="signup"),
]
