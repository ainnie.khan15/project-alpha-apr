from django.shortcuts import render
from accounts.forms import SingUpForm, LoginForm
from django.shortcuts import redirect
from django.contrib.auth import authenticate, logout
from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User


def signup(request):
    error = ""
    if request.method == "POST":
        form = SingUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                auth_login(request, user)
                messages.success(
                    request,
                    "Registration successful.",
                )
                return redirect("/projects/")
            else:
                error = "the password don't match."
                messages.error(
                    request,
                    "the password don't match.",
                )
        messages.error(
            request,
            "Unsuccessful registration. Invalid information.",
        )
    form = SingUpForm()
    return render(
        request=request,
        template_name="accounts/registration/signup.html",
        context={
            "form": form,
            "error": error,
        },
    )


def login(request):
    if request.method == "GET":
        form = LoginForm()
        return render(
            request,
            "accounts/login.html",
            {"form": form},
        )
    elif request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user:
                auth_login(
                    request,
                    user,
                )
                messages.success(
                    request,
                    f"Hi {username}, welcome back!",
                )
                return redirect("/projects")
        messages.error(
            request,
            "Invalid username or password",
        )
        return render(
            request,
            "accounts/login.html",
            {"form": form},
        )


def sign_out(request):
    logout(request)
    messages.success(
        request,
        "You have been logged out.",
    )
    return redirect("/accounts/login/")
