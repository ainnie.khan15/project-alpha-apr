from django.urls import path
from . import views
from .views import MyTasksView

app_name = "tasks"

urlpatterns = [
    path("create/", views.create_task, name="create_task"),
    path("mine/", MyTasksView.as_view(), name="show_my_tasks"),
]
