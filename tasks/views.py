from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from .forms import TaskForm
from django.views.generic import ListView
from .models import Task
from django.contrib.auth.mixins import LoginRequiredMixin


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("project_list")
    else:
        form = TaskForm()

    return render(request, "tasks/create_task.html", {"form": form})


class MyTasksView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/my_tasks.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


# Create your views here.
