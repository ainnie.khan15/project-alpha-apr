from django.urls import path
from .views import list_projects
from . import views

app_name = "projects"

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", views.show_project, name="show_project"),
]
