from django.views.generic import ListView
from .models import Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render


class ProjectListView(ListView):
    model = Project
    template_name = "projects/project_list.html"
    context_object_name = "projects"

    def get_queryset(self):
        # Get the current user
        user = self.request.user

        # Filter the projects by user owner
        queryset = super().get_queryset().filter(owner=user)

        return queryset


def list_projects(request):
    return login_required(ProjectListView.as_view())(request)


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "project_detail.html", context)
